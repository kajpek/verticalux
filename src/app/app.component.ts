import { Component, ViewChild, OnInit, HostListener } from '@angular/core';
import { Router, ActivatedRoute, Event as RouterEvent, NavigationStart,
  NavigationCancel, NavigationEnd, NavigationError, RoutesRecognized } from '@angular/router';
import { MatDrawer } from '@angular/material';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinct } from 'rxjs/operators';
import { trigger, state, animate, transition, style } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('fadeInAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('.3s', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate('.3s', style({ opacity: 0 }))
      ]),
    ])
  ]
})
export class AppComponent implements OnInit {

  @ViewChild('drawer') drawer: MatDrawer;
  public ex = false;
  public exShown = false;
  public policy = false;
  public terms = false;
  public cookies = false;
  public fixedMenu = false;
  private pageByScroll$ = fromEvent(window, 'scroll')
    .pipe(
      map(() => window.scrollY),
      filter(current => current >=  document.body.clientHeight - window.innerHeight)
    );

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    this.pageByScroll$.subscribe(val => {
      if (val > 50) {
        this.fixedMenu = true;
      } else {
        this.fixedMenu = false;
      }
      if (val >= 1000 && this.exShown === false) {
        setTimeout(() => {
          // this.ex = true;
        }, 1000);
      }
    });
  }

  close() {
    this.ex = false;
    this.exShown = true;
  }

  scroll(el) {
    this.scrollTo(el.offsetTop, () => {
      history.pushState(null, null, '#' + el.id);
    }, 1000);
    // el.scrollIntoView({behavior: 'smooth'});
  }

  open_policy() {
    this.policy = true;
  }
  close_policy() {
    this.policy = false;
  }

  open_terms() {
    this.terms = true;
  }
  close_terms() {
    this.terms = false;
  }

  close_cookies() {
    this.cookies = false;
    localStorage.setItem('cookies', 'accepted');
  }

  ngOnInit() {

    this.showCookies();

    this.router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event);
    });
  }

  showCookies() {

    if (localStorage.getItem('cookies') === null) {
      setTimeout(() => {
        this.cookies = true;
      }, 1000);
    }
  }

  navigationInterceptor = (event: RouterEvent) => {

    if (event instanceof NavigationEnd) {
      console.log('end');
    }

  }


  easeInOutQuad = function (t: number, b: number, c: number, d: number) {
      t /= d / 2;
      if (t < 1) {
          return c / 2 * t * t + b;
      }
      t--;
      return -c / 2 * (t * ( t - 2 ) - 1) + b;
  };

  scrollTo(to: number, callback: any, duration: number) {
      function move(amount: number) {
          document.documentElement.scrollTop = amount;
          (<Element>document.body.parentNode).scrollTop = amount;
          document.body.scrollTop = amount;
      }
      function position() {
          return document.documentElement.scrollTop || (<Element>document.body.parentNode).scrollTop || document.body.scrollTop;
      }
      const animationFrameReqFunc = window.requestAnimationFrame || window.webkitRequestAnimationFrame ||
      (<any>window).mozRequestAnimationFrame || function( callback: any ) { window.setTimeout(callback, 1000 / 60); };
      const start = position();
      const change = to - start;
      let currentTime = 0;
      const increment = 20;
      duration = (typeof(duration) === 'undefined') ? 500 : duration;
      const animateScroll = () => {
          //  increment the time
          currentTime += increment;
          //  find the value with the quadratic in-out easing function
          const val = this.easeInOutQuad(currentTime, start, change, duration);
          //  move the document.body
          move(val);
          //  do the animation unless its over
          if (currentTime < duration) {
              animationFrameReqFunc(animateScroll);
          } else {
              if (callback && typeof(callback) === 'function') {
                  //  the animation is done so lets callback
                  callback();
              }
          }
      };
      animateScroll();

  }
}
